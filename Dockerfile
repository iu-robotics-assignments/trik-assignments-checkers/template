FROM a1d4r/trik-checker

MAINTAINER Aidar Garikhanov a1d4r@yandex.ru

ARG task_name_arg=task
ARG solution_name_arg=solution
ENV task_name=$task_name_arg
ENV solution_name=$solution_name_arg

# "column -t" for text formatting
RUN apt-get install -y bsdmainutils

COPY . .

RUN chmod +x /setup.sh
RUN chmod +x /task/start_checker.sh

RUN ["/setup.sh"]