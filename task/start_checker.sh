#!/bin/bash

passed_message="Задание выполнено!"

if [ ! -f "/task/$task_name.qrs" ]; then
    echo "No .qrs file!"
    exit 1
fi

if [ ! -f "/task/$solution_name.js" ]; then
    echo "No solution file!"
    exit 1
fi

cd /task

echo "Running checker..."

/trikStudio-checker/checker.sh "$task_name.qrs" "$solution_name.js" > /dev/null 2>&1

if [[ $? -ne 0 ]]; then
    echo "Checker error"
    exit 1
fi

for report in /task/reports/$task_name/*; do
    echo -n "$(basename "$report"):"
    grep "message" "$report"
done > full_report

passed_tests=0
failed_tests=0

for report in /task/reports/$task_name/*; do
    if grep -q "$passed_message" "$report"
    then
        ((passed_tests++))
        echo "$(basename "$report"): passed"
    else
        ((failed_tests++))
        echo "$(basename "$report"): failed"
    fi
done > short_report

echo "===== Full report ====="
sort full_report -V | column -t
echo "===== Short report ====="
sort short_report -V | column -t

total_tests=$((passed_tests + failed_tests))
echo "Total score: $passed_tests/$total_tests"

if [[ failed_tests -ne 0 ]]; then
    exit 1
fi
